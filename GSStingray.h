//
//  GSStingray.h
//  GSStingray
//
//  Created by Dan Kalinin on 8/23/20.
//

#import <GSStingray/StgMain.h>
#import <GSStingray/StgSchema.h>
#import <GSStingray/StgClient.h>
#import <GSStingray/StgBrowser.h>
#import <GSStingray/StgInit.h>

FOUNDATION_EXPORT double GSStingrayVersionNumber;
FOUNDATION_EXPORT const unsigned char GSStingrayVersionString[];
