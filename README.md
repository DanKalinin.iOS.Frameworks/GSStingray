# GSStingray

STB configuration SDK for iOS.

- [Installation](#installation)
- [API](#api)
    - [Client](#client)
    - [Browser](#browser)
- [Examples](#examples)
    - [Turn the power on](#turn-the-power-on)
    - [Get channel list](#get-channel-list)
    - [Switch the channel](#switch-the-channel)

## Installation

Add these lines to your `Podfile` and run `pod install` command.

```ruby
install! "cocoapods",
  :preserve_pod_file_structure => true

pod "GLibExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/GLibExt.git", :tag => "1.1", :submodules => true
pod "SoupExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/SoupExt.git", :tag => "1.1", :submodules => true
pod "GnuTLSExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/GnuTLSExt.git", :tag => "1.1", :submodules => true
pod "GLibNetworkingExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/GLibNetworkingExt.git", :tag => "1.1", :submodules => true
pod "JsonExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/JsonExt.git", :tag => "1.1", :submodules => true
pod "AvahiExt", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/AvahiExt.git", :tag => "1.1", :submodules => true
pod "GSStingray", :git => "https://gitlab.com/GSLabs.iOS.Frameworks/GSStingray.git", :tag => "1.1", :submodules => true
```

## API

The API consists of 2 main interfaces - Client and Browser.

### Client

`StgClient` class provides high level API for making HTTP requests to the STB - PowerSet, ChannelsGet, ChannelSet, etc. Each method has its synchronous and asynchronous version. Synchronous version ends with `Sync` suffix and blocks the current thread until request is completed. Asynchronous version ends with `Async` suffix and invokes a completion block when request is completed. All completion blocks are dispatched to the application's main thread.

```objectivec
StgClient *client = [StgClient clientWithBase:@"http://192.168.0.1:50000" userAgent:@"stingray"];
```

### Browser

`StgBrowser` class discovers the STBs in local network and resolves their IP addresses. Resolved addresses can be used by `StgClient` instances for making HTTP requests to the STBs.

```objectivec
self.browser = [StgBrowser browserWithType:@"_stingray-remote._tcp"];
[self.browser.delegates addObject:self];
[self.browser start];

- (void)stgBrowser:(StgBrowser *)sender endpointFound:(StgEndpoint *)endpoint {
    StgClient *client = [StgClient clientWithEndpoint:endpoint userAgent:@"stingray"];
}
```

## Examples

Common use cases are described below.

### Turn the power on

Before making requests, ensure that STB is out of standby mode by turning it explicitly on.

```objectivec
[client powerSetAsync:YES completion:^(NSError *error) {
    if (error == nil) {
        // Success
    } else {
        NSLog(error.localizedDescription);
    }
}];
```

### Get channel list

Obtain the list of available channels.

```objectivec
[client channelsGetAsync:^(NSArray<StgChannel *> *channels, NSError *error) {
    if (channels == nil) {
        NSLog(error.localizedDescription);
    } else {
        // Success
    }
}];
```

### Switch the channel

Make the channel active.

```objectivec
StgChannel *channel = StgChannel.new;
channel.channelListId = @"TV";
channel.channelNumber = 2;

[client channelSetAsync:channel completion:^(NSError *error) {
    if (error == nil) {
        // Success
    } else {
        NSLog(error.localizedDescription);
    }
}];
```
