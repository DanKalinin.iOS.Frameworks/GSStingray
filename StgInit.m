//
//  StgInit.m
//  GSStingray
//
//  Created by Dan on 26.10.2021.
//

#import "StgInit.h"

@implementation StgInit

+ (void)initialize {
    (void)NseInit.class;
    (void)SoeInit.class;
    (void)TlsInit.class;
    (void)GneInit.class;
    (void)JseInit.class;
    (void)AvhInit.class;
    
    stg_init();
}

@end
