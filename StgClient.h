//
//  StgClient.h
//  GSStingray
//
//  Created by Dan on 12.11.2021.
//

#import "StgMain.h"
#import "StgSchema.h"

@interface StgClient : NseObject

@property STGSOESession *object;
@property NSString *base;
@property NSString *userAgent;

+ (instancetype)clientWithBase:(NSString *)base userAgent:(NSString *)userAgent;
+ (instancetype)clientWithEndpoint:(StgEndpoint *)endpoint userAgent:(NSString *)userAgent;

- (void)abort;

#pragma mark - PowerSet

- (BOOL)powerSetSync:(BOOL)on error:(NSError **)error;

typedef void (^StgClientPowerSetAsyncCompletion)(NSError *error);
- (void)powerSetAsync:(BOOL)on completion:(StgClientPowerSetAsyncCompletion)completion;

#pragma mark - ChannelsGet

- (NSArray<StgChannel *> *)channelsGetSync:(NSError **)error;

typedef void (^StgClientChannelsGetAsyncCompletion)(NSArray<StgChannel *> *channels, NSError *error);
- (void)channelsGetAsync:(StgClientChannelsGetAsyncCompletion)completion;

#pragma mark - ChannelSet

- (BOOL)channelSetSync:(StgChannel *)channel error:(NSError **)error;

typedef void (^StgClientChannelSetAsyncCompletion)(NSError *error);
- (void)channelSetAsync:(StgChannel *)channel completion:(StgClientChannelSetAsyncCompletion)completion;

@end
